<html>
    <head>
        <title>App Name - @yield('title')</title>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="{{ asset('js/header.js') }}" defer></script>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @yield('jsCode')
        @yield('cssCode')
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
         <div class="container-fluid">
           <a class="navbar-brand" href="/">TestInky</a>
           <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
             <span class="navbar-toggler-icon"></span>
           </button>
           <div class="collapse navbar-collapse" id="navbarSupportedContent">
               @auth
                   <a class="btn btn-outline-error" href="{{ route('logout') }}">logOUt</a>
               @endauth
               @guest
                   <a class="btn btn-outline-success" href="/login">{{__('custom.login')}}</a>
                   <a class="btn btn-outline-success" href="/register">{{__('custom.register')}}</a>
               @endguest
                   <div class="text-right m-3">
                       <select class="form-select form-select-lg mb-3" name="language" style="width: 10%;" onchange="">
                           <option value="en" {{ Session::get('language') == 'en' ? 'selected' : '' }}>English</option>
                           <option value="fr" {{ Session::get('language') == 'fr' ? 'selected' : '' }}>Francais</option>
                       </select>
                   </div>
           </div>
         </div>
        </nav>
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>
