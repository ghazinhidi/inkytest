@extends('layouts.app')
@section('title')
    Test Inky
@endsection
@section('content')
    @if(isset($canShort) and $canShort)
        <form method="POST" action="{{ route('store.link') }}">
            @csrf
            <div class="row">
                <div class="col-6">
                    <input type="text" class="form-control" name="url">
                </div>
                <div class="col-6">
                    <button type="submit" class="btn btn-primary">{{__('custom.shorten')}}</button>
                </div>
            </div>
        </form>
        @if(isset($shortUrl))
            <div class="alert alert-success">
                {{ $shortUrl }}
            </div>
        @endif
    @else
        <div class="alert alert-danger">
            {{__('custom.max_links_for_user')}}
        </div>
    @endif

    <div class="container table-responsive py-5">
        <table class="table table-bordered table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col">{{__('custom.url')}}</th>
                <th scope="col">{{__('custom.short_url')}}</th>
                <th scope="col">{{__('custom.creation_date')}}</th>
                <th scope="col">{{__('action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($links as $link)
                <tr>
                    <td>{{$link->url}}</td>
                    <td>{{$link->shortUrl}}</td>
                    <td>{{$link->created_at}}</td>
                    <td>
                        <form method="GET" action="{{ route('deleteLink',$link->id) }}">
                            <button type="submit" class="btn btn-red">{{__('custom.delete')}}</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>@endsection
