<?php

return [

    'delete' => 'delete',
    'confirm_password' => 'Confirm Password',
    'confirm_password_continuing' => 'Please confirm your password before continuing.',
    'password' => 'Password',
    'forgot_password' => 'Forgot Your Password?',
    'unlogged_user_message' => 'login to use this app',
    'shorten' => 'shorten',
    'max_links_for_user' => 'you have exceeded the maximum number of urls',
    'url' => 'links',
    'short_url' => 'short links',
    'creation_date' => 'creation date',
    'verify_mail' => 'Verify Your Email Address',
    'fresh_verification_link' => 'A fresh verification link has been sent to your email address.',
    'before_proceeding' => 'Before proceeding, please check your email for a verification link.',
    'did_not_receive_mail' => 'If you did not receive the email',
    'request_another' => "click here to request another",
    'register' => "Register",
    'firstName' => "First name",
    'lastName' => "Last name",
    'Password' => "Password",
    'login' => "Login",
    'remember_me' => "Remember Me",
    'forgot_password' => "Forgot Your Password?",



];
