<?php

return [

    'delete' => 'supprimer',
    'confirm_password' => 'Confirmer mot de passe',
    'confirm_password_continuing' => 'Merci de confirmer votre mot de passe avant de contunier ',
    'password' => 'Mot de passe',
    'forgot_password' => 'Mot de passe oublié ?',
    'unlogged_user_message' => 'connectez vous pour utliser cette application',
    'shorten' => 'raccourcir',
    'max_links_for_user' => 'vous avez deppasé le nombre maximum de urls',
    'url' => 'liens',
    'short_url' => 'mini liens',
    'creation_date' => 'date creation',
    'verify_mail' => 'Verifier votre adresse mail',
    'fresh_verification_link' => 'Un nouveau lien de vérification a été envoyé à votre adresse e-mail.',
    'before_proceeding' => 'Avant de continuer, veuillez vérifier votre e-mail pour un lien de vérification.',
    'did_not_receive_mail' => "Si vous n'avez pas reçu le mail",
    'request_another' => "cliquez ici pour en demander un autre",
    'register' => "S'inscrire",
    'firstName' => "nom",
    'lastName' => "prenom",
    'Password' => "mot de passe",
    'login' => "Connexion",
    'remember_me' => "Souviens moi",
    'forgot_password' => "Mot de passe oublié?",





];
