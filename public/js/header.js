function showContainer(nameContainer,view){
    $('#container'+nameContainer).append(view);
}
$(document).ready(function() {
    $('select[name=language]').change(function() {
        var lang = $(this).val();
        window.location.href = "/language-change?language="+lang;
    });
});
