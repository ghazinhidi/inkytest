<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinksLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links_logs', function (Blueprint $table) {
            //Le temps d'accès existe par defaut (champs created_at)
            $table->id();
            $table->string('url');
            $table->string('ip');
            $table->string('pays');
            $table->string('userAgent');
            $table->integer('idUser')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links_logs');
    }
}
