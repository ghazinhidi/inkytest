<?php

namespace App\Services;

use App\Models\Links;
use App\Models\LinksLogs ;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use Stevebauman\Location\Facades\Location;

class ServiceLinks
{

    public function generateShortUrl()
    {

        return url('/').'/short/'. Str::random(7);
    }

    /**
     * @param $idUser
     * @return \Illuminate\Support\Collection|null
     */
    public function getlinksForUser($idUser,$count = false){
        $typeData = $count ?'count':'get';
        if ($idUser){
            return DB::table(Links::TABLE)
                ->where('idUser', $idUser)
                ->{$typeData}();
        }
        return null;
    }
    function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }
    public function getFullUrl($short)
    {
        $result = DB::table(Links::TABLE)->where('shortUrl','like','%'. $short)->first();
        if (isset($result->url)){
            return $result->url;
        }
        return null;
    }
    public function addNewLinkWithControl($url)
    {
        if (Links::count() == Links::MAX_TOTAL_LINKS){
            DB::table('links')->where('created_at',Links::min('created_at'))->delete();
        }
        $shortUrl = $this->generateShortUrl();
        $add = Links::create([
            'idUser' => Auth::user()->id,
            'url' => $url,
            'shortUrl' => $shortUrl,
        ]);
        if ($add){
            return $shortUrl;
        }

        return null;
    }
    public function getFullUrlWithLog($short)
    {
        if ($url = $this->getFullUrl($short)) {
             LinksLogs::create([
                'idUser' => Auth::user()->id ?? 0,
                'url' => $url,
                'ip' => Request::ip(),
                'pays' => $this->getPays(),
                'userAgent' => Request::header('User-Agent')
            ,
            ]);
            return $url;
        }

        return null;
    }
    public function getPays(){

        $location = Location::get(Request::ip());
        if ($location){
            return $location->countryName;
        }
        return 'local';
    }
}
