<?php

namespace App\Services;

use App\Models\Links;
use Illuminate\Support\Facades\Auth;

class Rights
{
    const CAN_ADD_SHORTLINK = 'CAN_ADD_SHORTLINK';
    static function hasRight(string $right)
    {
        switch ($right)
        {
            case self::CAN_ADD_SHORTLINK :
                return app(ServiceLinks::class)->getlinksForUser(Auth::user()->id,true) < Links::MAX_LINKS_PER_USER;
        }
    }
}
