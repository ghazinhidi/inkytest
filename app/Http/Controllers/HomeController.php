<?php

namespace App\Http\Controllers;

use App\Models\Links;
use App\Services\Rights;
use App\Services\ServiceLinks;
use http\Env\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Link;
use Stevebauman\Location\Facades\Location;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(ServiceLinks $serviceLinks)
    {
        if (!Auth::user()){
            return view('homeUnLogged');
        }
        return view('homeLogged',[
            'links' => $serviceLinks->getlinksForUser(Auth::user()->id),
            'canShort'=> Rights::hasRight(Rights::CAN_ADD_SHORTLINK)
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirect(Request $request){
        if (isset($request['short']) && $url = app(ServiceLinks::class)->getFullUrlWithLog($request['short'])){
            return redirect($url);
        }

        return redirect("/");
    }

    /**
     * @param Request $request
     * @param ServiceLinks $serviceLinks
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function store(Request $request,ServiceLinks $serviceLinks)
    {
        if (Auth::user()){
            $shortUrl = $serviceLinks->addNewLinkWithControl($request['url']);

            return view('homeLogged',[
                'links' => $serviceLinks->getlinksForUser(Auth::user()->id),
                'shortUrl'=>$shortUrl,
                'canShort'=> Rights::hasRight(Rights::CAN_ADD_SHORTLINK)
            ]);
        }

        return redirect("/");
    }
    public function deleteLink(Request $request)
    {

        if (isset($request['idLink']) && Auth::user()){
            Links::destroy($request['idLink']);
        }

        return redirect("/");
    }
}
