<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Links extends Model
{
    const  TABLE='links';
    protected $fillable = [
        'idUser',
        'url',
        'shortUrl',
    ];

    const MAX_LINKS_PER_USER = 5;
    const MAX_TOTAL_LINKS = 20;
}
