<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LinksLogs extends Model
{
    use HasFactory;
    const  TABLE='links_logs';
    protected $fillable = [
        'idUser',
        'url',
        'ip',
        'pays',
        'userAgent',
    ];
}
