<?php

use App\Http\Controllers\CondidatController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[HomeController::class,'index'])->name('index');
Route::get('/short/{short}',[HomeController::class,'redirect'])->name('redirect');
Route::post('/',[HomeController::class,'store'])->name('store.link');
Route::get('/delete/{idLink}',[HomeController::class,'deleteLink'])->name('deleteLink');

Route::get('/language-change', [LanguageController::class, 'changeLanguage'])->name('changeLanguage');

Route::get('/users',[UsersController::class,'index'])->name('users.index');
Route::get('/users/adduser',[UsersController::class,'showAddUser'])->name('users.showAddUser');
Route::get('showCondidats',[CondidatController::class,'showCondidats'])->name('condidat.showCondidats');




Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
